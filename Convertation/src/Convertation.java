import java.text.DecimalFormat;

public class Convertation {

    double gelToUSD(double gel){

        DecimalFormat df = new DecimalFormat("#.##");

        return Double.parseDouble(df.format(gel / 3.0648));

    }

}
