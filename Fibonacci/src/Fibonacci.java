public class Fibonacci {

    int fib(int n){

        if (n > 0 && n <= 2){

            return 1;

        }

        else if (n > 2){

            int f[] = new int[n + 1];
            f[0] = 1;
            f[1] = 1;

            for (int i = 2; i < n; i++) {

                f[i] = f[i - 1] + f[i - 2];

            }

            return f[n - 1];

        }

        else {

            return 0;
        }

    }
}
